/**
 * UsuarioController
 *
 * @description :: Server-side logic for managing usuarios
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var _ = require('lodash');

module.exports = {

  /**
   * `UsuarioController.registro()`
   */
  registro: function (req, res) {
    // TODO mostrar el formulario
    res.view(null, {
      title: 'Registro de usuario'
    });
  },


  /**
   * `UsuarioController.registrar()`
   */
  registrar: function (req, res) {
    var usuarioRegistrado = true,
      entrada = req.body;

    if (entrada.password !== entrada.repeatPassword) {
      return res.view('usuario/registro', {
        title: 'Registro de usuario',
        errores: { password: 'No coinciden las contrasenas' }
      });
    }

    Usuario.create(entrada).exec(function (errores, model) {
      var erroresVista = {};
      if (errores) {
        // Ver si es un error de validacion.
        console.log(errores.invalidAttributes);
        if (errores.invalidAttributes) {
          if (errores.invalidAttributes.nombre) {
            erroresVista.nombre = 'Debe introducir un nombre.';
          }
          if (errores.invalidAttributes.email) {
            if (
              _.any(
                errores.invalidAttributes.email,
                function (error) {
                  return error.rule === 'unique';
                }
              )
            ) {
              erroresVista.email = 'Ya existe un usuario con este correo';
            } else {
              erroresVista.email = 'Introduzca un email valido.';
            }
          }
          if (errores.invalidAttributes.password) {
            erroresVista.password = 'Debe introducir una contrasena de al menos 6 caracteres.';
          }
        }
        return res.view('usuario/registro', {
          title: 'Registro de usuario',
          errores: erroresVista,
          datos: entrada
        });
      } else {
        console.log('Usuario ' + model.nombre + ' creado satisfactoriamente!');
        return res.view(null, {
          title: 'Registro satisfactorio',
          datos: entrada
        });
      }
    });
  },


  /**
   * `UsuarioController.conectar()`
   */
  conectar: function (req, res) {
    return res.json({
      todo: 'conectar() is not implemented yet!'
    });
  }

};

